<?php

class MsCodes {

    public function __construct() {
        global $wpdb;
        $this->table = $wpdb->prefix . "kanzi_codes";
        $this->wpdb = $wpdb;
    }

    public function getAll() {
        $sql = "SELECT * FROM $this->table";
        $data = $this->wpdb->get_results($sql, ARRAY_A);
        return $data;
    }

    public function getSingle($code) {
        $sql = "SELECT * FROM $this->table WHERE code = '$code'";
        $data = $this->wpdb->get_results($sql);
        return $data[0];
    }

    public function updateSingle($data) {
        return $this->wpdb->update($this->table, array( 'status' => $data['status']), ['id_kanzi_codes' => $data['id']]);
    }

    public function saveData($data) {
        return $this->wpdb->insert($this->table, $data);
    }

    public function deleteData($data) {
        $this->wpdb->delete( $this->table, array( 'id_kanzi_codes' => $data ) );
    }
}

?>
