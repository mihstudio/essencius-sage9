<?php

class MsUserVouchers {

    public function __construct() {
        global $wpdb;
        $this->table = $wpdb->prefix . "kanzi_user_vouchers";
        $this->table2 = $wpdb->prefix . "kanzi_vouchers";
        $this->wpdb = $wpdb;
    }

    public function getAll($id) {
        $sql = "SELECT $this->table2.`code` FROM $this->table 
        LEFT JOIN $this->table2 ON $this->table2.`id_kanzi_vouchers` = $this->table.`id_kanzi_vouchers`
        WHERE $this->table.`id_user`=$id";
        $data = $this->wpdb->get_results($sql, ARRAY_A);
        return $data;
    }

    public function saveData($data) {
        return $this->wpdb->insert($this->table, $data);
    }

    public function deleteData($data) {
        $this->wpdb->delete( $this->table, array( 'id_kanzi_user_vouchers' => $data ) );
    }
}

?>
