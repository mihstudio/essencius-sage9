<?php

class MsVouchers {

    public function __construct() {
        global $wpdb;
        $this->table = $wpdb->prefix . "kanzi_vouchers";
        $this->wpdb = $wpdb;
    }

    public function getSingle() {
        $sql = "SELECT * FROM $this->table WHERE status = 1 ORDER BY `id_kanzi_vouchers` ASC LIMIT 1";
        $data = $this->wpdb->get_results($sql);
        if($data) {
            return $data[0];
        }
    }

    public function updateSingle($data) {
        return $this->wpdb->update($this->table, array( 'status' => $data['status']), ['id_kanzi_vouchers' => $data['id']]);
    }

    public function saveData($data) {
        return $this->wpdb->insert($this->table, $data);
    }

    public function deleteData($data) {
        $this->wpdb->delete( $this->table, array( 'id_kanzi_vouchers' => $data ) );
    }
}

?>
