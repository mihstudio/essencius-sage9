<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Helper
 *
 * @author Konrad
 */
class Helper {
    
    public function textToArray($text){
        $textAr = explode("\n", $text);
        return array_filter($textAr, 'trim');
    }
}
