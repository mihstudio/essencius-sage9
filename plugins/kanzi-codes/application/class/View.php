<?php

class View {

    public function __construct($module) {
        $this->module = $module;
    }

    public function getView($name, $data = array()) {

        $plugin_name = plugin_basename(__FILE__);
        $plugin_name = explode('/', $plugin_name);
        $plugin_name = $plugin_name[0];
        
        $path = realpath('');
        $path_array = explode("/", $path);
        $count_path = sizeof($path_array) - 1;
        if($path_array[$count_path] == 'wp-admin'){
            unset($path_array[$count_path]);
        }
        $path = implode('/', $path_array);
        $url = $path . '/wp-content/plugins/' . $plugin_name . '/application/modules/' . $this->module . '/views/' . $name . '.php';

        ob_start();
        extract($data);
        require $url;
        ob_end_flush();
    }

}
