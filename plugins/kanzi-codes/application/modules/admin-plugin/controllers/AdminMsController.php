<?php

class AdminMsController {

    public $helper;

    function __construct() {
        wp_enqueue_style('plugin', MRP_URL . 'assets/css/style-admin.css', array(), null, 'all');
        $this->helper = new Helper();
    }

    function addCodes() {
        $modelCodes = new MsCodes();
        
        if (!empty($_POST)) {
            if(isset($_POST['add_codes']) && !empty($_POST['codes'])) {
                foreach($this->helper->textToArray($_POST['codes']) as $row) {
                    $data = [
                        'code' => trim($row),
                        'status' => true,
                    ];
                    $modelCodes->saveData($data);
                }
            }

            if(isset($_POST['add_vouchers'])) {

            }
        }

        $pluginView = new View('admin-plugin');
        $pluginView->getView('admin-add-codes');
    }

}

?>
