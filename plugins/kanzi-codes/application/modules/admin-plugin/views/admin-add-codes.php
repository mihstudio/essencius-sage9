<div class="admin-map-panel">
    <h1>Add codes / vouchers</h1>
    <form action="admin.php?page=kanzi-codes-admin" method="POST">
    <table class="form-table" role="presentation">
        <tbody>
            <tr>
                <th scope="row">Codes</th>
                <td>
                <fieldset>
                <legend class="screen-reader-text">
                    <span>Codes</span>
                </legend>
                <p>One code per row.</p>
                <p>
                <textarea name="codes" rows="10" cols="50" class="large-text code"></textarea>
                </p>
                </fieldset>
                </td>
            </tr>
        </tbody>
    </table>
    <p class="submit">
        <input type="submit" id="submit" class="button button-primary" value="Add codes" name="add_codes">
    </p>
    </form>

    <form action="admin.php?page=kanzi-codes-admin" method="POST">
    <table class="form-table" role="presentation">
        <tbody>
            <tr>
                <th scope="row">
                Vouchers (.xlsx, .xls)
                </th>
                <td>
                    <input type="file" id="avatar" name="vouchers_file" accept=".xlsx, .xls">
                </td>
            </tr>
        </tbody>
    </table>
    <p class="submit">
        <input type="submit" id="submit" class="button button-primary" value="Add vouchers" name="add_vouchers">
    </p>
    </form>
</div>