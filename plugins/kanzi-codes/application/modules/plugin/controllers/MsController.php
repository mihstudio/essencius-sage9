<?php

class MsController {
    private $codesModel;
    private $vouchersModel;
    private $userCodesModel;

    public function __construct() {
        $this->codesModel = new MsCodes();
        $this->userVouchersModel = new MsUserVouchers();
        $this->vouchersModel = new MsVouchers();
    }

    function showPlugin() {
        $pluginView = new View('plugin');

        $data = array(
            'count' => (int) get_field('codeCount', 'user_' . get_current_user_id()),
            'vouchers' => $this->getUserVouchers()
        );
        $pluginView->getView('show', $data);
    }

    function addUserCode($code) {
        $checkCode = $this->checkCode($code);
        $vmsg = null;
        if($checkCode) {
            $data = [
                'id_code' => (int) $checkCode,
                'id_user' => get_current_user_id()
            ];
            $count = (int) get_field('codeCount', 'user_' . $data['id_user']);
            $countAll = (int) get_field('codeCountAll', 'user_' . $data['id_user']);
            $count++;
            update_field('codeCount', $count, 'user_' . $data['id_user']);
            $countAll++;
            update_field('codeCountAll', $countAll, 'user_' . $data['id_user']);

            if($count > 5) {
                update_field('codeCount', 1, 'user_' . $data['id_user']);
            }
            if($count == 5) {
                $userVoucher = $this->addUserVoucher();
                if($userVoucher) {
                    $vmsg = 'voucher!';
                    $this->sendUserVoucherEmail($userVoucher);
                }
            }
            
            $this->disableCode($code);
            $success = true;
            $message = 'Koden blev tilføjet med succes';
        } else {
            $success = false;
            $message = 'Forkert eller inaktiv kode';
        }

        return $data = [
            'success' => $success,
            'message' => $message,
            'vmsg' => $vmsg,
            'count' => (int) get_field('codeCount', 'user_' . get_current_user_id())
        ];
    }

    function checkCode($code) {
        $singleCode = $this->codesModel->getSingle(trim($code));
        if(!empty($singleCode) && $singleCode->status) {
            return $singleCode->id_kanzi_codes;
        } else {
            return false;
        }
    }

    function disableCode($code) {
        $singleCode = $this->codesModel->getSingle(trim($code));
        $data = [
            'id' => $singleCode->id_kanzi_codes,
            'status' => 0
        ];
        return $this->codesModel->updateSingle($data);
    }

    function disableVoucher($id) {
        $data = [
            'id' => $id,
            'status' => 0
        ];
        return $this->vouchersModel->updateSingle($data);
    }

    function addUserVoucher() {
        $id_user = get_current_user_id();
        $voucher = $this->vouchersModel->getSingle();
        if($voucher) {
            $data = [
                'id_user' => get_current_user_id(),
                'id_kanzi_vouchers' => (int) $voucher->id_kanzi_vouchers
            ];
            if($this->userVouchersModel->saveData($data)) {
                if($this->disableVoucher($data['id_kanzi_vouchers'])) {
                    return $voucher->code;
                } else {
                    return false;
                }
            }
        }
    }

    function getUserVouchers() {
        return $this->userVouchersModel->getAll(get_current_user_id());
    }

    function sendUserVoucherEmail($voucher) {
        $user = wp_get_current_user();

        $to = $user->user_email;
        $subject = 'VÆRDIKUPON';
        $body = "Tillykke, du kan nu designe din egne kogebog gratis. "
        ."VÆRDIKUPON: $voucher";
        $headers = array('Content-Type: text/html; charset=UTF-8');
        
        wp_mail( $to, $subject, $body, $headers );
    }
}

?>
