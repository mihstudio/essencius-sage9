<section class="coderApple">
  <img class="coderApple__bg" src="<?= plugins_url( '/' ) ?>/kanzi-codes/assets/images/apple.svg" />
  <div class="coderApple__count"><span class="coderApple__count__codes"><?= $data['count'] ?></span> koder</div>
</section>
<a href="#" type="button" class="btn btn--primary btn--coder" data-toggle="modal" data-target="#modalAddCode">
    indtast koder
</a>
<a href="#" type="button" class="btn btn--secondary btn--coder" data-toggle="modal" data-target="#modalShowVoucher">
    Mine Værdikuponer
</a>

<div class="modal fade modalAuth" id="modalAddCode">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content modalContent">
      <div class="modal-header modalHeader">
        <button type="button" class="close modalHeader__close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body modalBody modalBody--code">
        <div class="loader">
          <img src="<?= plugins_url( '/' ) ?>/kanzi-codes/assets/images/loader.gif" />
        </div>
        <section class="authForm mt-4">
            <div class="authForm__message small mb-2"></div>
            <input type="text" class="authForm__input authForm__input--code" name="code" />
            <a href="#" class="btn btn--primary btn--auth btnAddCode" data-name="add_code">Tilføj kode</a>
        </section>
      </div>
    </div>
  </div>
</div>

<div class="modal fade modalAuth modalShowVoucher" id="modalShowVoucher">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content modalContent">
      <div class="modal-header modalHeader">
        <button type="button" class="close modalHeader__close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body modalBody modalBody--voucher">
        <h2 class="modalBody__header">Mine værdikuponer</h2>
        <?php if($data['vouchers']) { ?>
          <div class="modalShowVoucher__description">
            Tillykke, du kan nu designe din egne kogebog gratis.
          </div>
          <?php foreach($data['vouchers'] as $row) { ?>
            <div class="modalShowVoucher__voucher"><?= $row['code'] ?></div>
          <?php } ?>
        <?php } else { ?>
          <div class="modalShowVoucher__description">
            Du skal samle 5 koder for at få en værdikupon, så du kan designe din egen kogebog gratis.
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    (function($) {
        $('.modalHeader__close').on('click', function(e) {
          $('.authForm__message').html('');
        });
        $('.btnAddCode').on('click', function(e) {
            e.preventDefault();
            let code = $('.authForm__input--code').val();
            let codeCount = $('.coderApple__count__codes');
            let message = $('.authForm__message');
            var msg;
            if(code.length > 0) {
              $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                async: true,
                dataType: 'json',
                data: {
                    'action': $(this).attr('data-name'),
                    'code': code
                },
                success: function (data) {
                    if (data) {
                      codeCount.html(data.count);
                      if(data.success) {
                        msg = '<div class="text-success">';
                      } else {
                        msg = '<div class="text-danger">';
                      }
                        msg += data.message + '</div>';
                        message.html(msg);
                    }
                },
                beforeSend: function(){
                  $(".loader").show();
                },
                complete: function(){
                  $(".loader").hide();
                  $(".authForm__input--code").val('');
                },
              });
            }
        });
    })(jQuery);
</script>