<?php

/*
  Plugin Name: Kanzi Codes
  Version: 1.0
  Description: Wordpress plugin for Kanzi by Appwise
  Author: Appwise
  Author URI: https://appwise.pl/
  License: GPLv2
  Text Domain: Appwise
 */

define('MRP_URL', plugin_dir_url(__FILE__));
define('MRP_PATH', plugin_dir_path(__FILE__));

require MRP_PATH . 'application/models/MsCodes.php';
require MRP_PATH . 'application/models/MsVouchers.php';
require MRP_PATH . 'application/models/MsUserVouchers.php';
require MRP_PATH . 'application/class/View.php';
require MRP_PATH . 'application/class/Helper.php';
require MRP_PATH . 'application/modules/plugin/controllers/MsController.php';
require MRP_PATH . 'application/modules/admin-plugin/controllers/AdminMsController.php';

/**
 * The connection to the database for the plugin
 *
 * @global $wpdb
 *
 * install plugin
 */
function plugin_install() {

    global $wpdb;
    $table = "kanzi_codes";
    $table2 = "kanzi_vouchers";
    $table3 = "kanzi_user_vouchers";

    $table_db_version = "1.0";

    if ($wpdb->get_var("SHOW TABLES LIKE '" . $wpdb->prefix . $table . "'") != $wpdb->prefix . $table) {

        $query1 = "CREATE TABLE " . $wpdb->prefix . $table . " (
		id_kanzi_codes int(11) NOT NULL,
                code VARCHAR(255) NOT NULL UNIQUE,
                status TINYINT(1) NOT NULL DEFAULT '1',
                PRIMARY KEY (id_kanzi_codes)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        
        $query2 = "ALTER TABLE " . $wpdb->prefix . $table . "
		MODIFY id_" . $table . " int(11) NOT NULL AUTO_INCREMENT;";
        
        $wpdb->query($query1);
        $wpdb->query($query2);

        add_option($table . "_db_version", $table_db_version);
        add_option($table . '_speed', '2000');
        add_option($table . '_interval', '2000');
        add_option($table . '_type', 'vertical');
    }

    if ($wpdb->get_var("SHOW TABLES LIKE '" . $wpdb->prefix . $table2 . "'") != $wpdb->prefix . $table2) {

        $query3 = "CREATE TABLE " . $wpdb->prefix . $table2 . " (
		id_kanzi_vouchers int(11) NOT NULL,
                code VARCHAR(255) NOT NULL UNIQUE,
                status TINYINT(1) NOT NULL DEFAULT '1',
                PRIMARY KEY (id_kanzi_vouchers)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        
        $query4 = "ALTER TABLE " . $wpdb->prefix . $table2 . "
		MODIFY id_" . $table2 . " int(11) NOT NULL AUTO_INCREMENT;";
        
        $wpdb->query($query3);
        $wpdb->query($query4);

        add_option($table2 . "_db_version", $table_db_version);
        add_option($table2 . '_speed', '2000');
        add_option($table2 . '_interval', '2000');
        add_option($table2 . '_type', 'vertical');
    }

    if ($wpdb->get_var("SHOW TABLES LIKE '" . $wpdb->prefix . $table3 . "'") != $wpdb->prefix . $table3) {

        $query5 = "CREATE TABLE " . $wpdb->prefix . $table3 . " (
		id_kanzi_user_vouchers int(11) NOT NULL,
		        id_user int(11) NOT NULL,
		        id_kanzi_vouchers int(11) NOT NULL,
                PRIMARY KEY (id_kanzi_user_vouchers)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        
        $query6 = "ALTER TABLE " . $wpdb->prefix . $table3 . "
		MODIFY id_" . $table3 . " int(11) NOT NULL AUTO_INCREMENT;";
        
        $wpdb->query($query5);
        $wpdb->query($query6);

        add_option($table3 . "_db_version", $table_db_version);
        add_option($table3 . '_speed', '2000');
        add_option($table3 . '_interval', '2000');
        add_option($table3 . '_type', 'vertical');
    }
}

register_activation_hook(__FILE__, 'plugin_install');

/**
 * uninstall plugin
 */
function plugin_uninstall() {

    global $wpdb;
    $table = $wpdb->prefix . "kanzi_codes";
    $table2 = $wpdb->prefix . "kanzi_vouchers";
    $table3 = $wpdb->prefix . "kanzi_user_vouchers";

    $query = 'DROP TABLE ' . $table;
    $query2 = 'DROP TABLE ' . $table2;
    $query3 = 'DROP TABLE ' . $table3;

    $wpdb->query($query);
    $wpdb->query($query2);
    $wpdb->query($query3);
}

register_deactivation_hook(__FILE__, 'plugin_uninstall');

function AdminPlugin() {
    $adminMsController = new AdminMsController();
    $adminMsController->addCodes();
}

function AdminPluginList() {
    $adminMsController = new AdminMsController();
    $adminMsController->addCodes();
}

function pluginMenu() {
    add_menu_page('plugin-menu', 'Kanzi Codes', 'administrator', 'kanzi-codes-admin', 'AdminPlugin', 'dashicons-tickets-alt');
    add_submenu_page( 'kanzi-codes-admin', 'Add codes / vouchers', 'Add codes / vouchers', 'manage_options', 'kanzi-codes-admin');
    // add_submenu_page( 'kanzi-codes-admin', 'User vouchers', 'User vouchers', 'manage_options', 'kanzi-codes-admin-user-vouchers', 'AdminPluginList');
}

add_action('admin_menu', 'pluginMenu');

function showPlugin() {
    $pluginController = new MsController();
    $pluginController->showPlugin();
}

function add_code() {
    $pluginController = new MsController();
    if (isset($_POST) && get_current_user_id()) {
        echo json_encode($pluginController->addUserCode(sanitize_text_field($_POST['code'])));
    }
    wp_die();
}

add_action('wp_ajax_add_code', 'add_code');
add_action('wp_ajax_nopriv_add_code', 'add_code');

add_shortcode('kanzi-codes-profile', 'showPlugin');
?>
