<?php
$auth = SwpmAuth::get_instance();
$setting = SwpmSettings::get_instance();
$password_reset_url = $setting->get_value('reset-page-url');
$join_url = $setting->get_value('join-us-page-url');
?>
<div class="swpm-login-widget-form">
    <form class="authForm loginForm" id="swpm-login-form" name="swpm-login-form" method="post" action="">
        <div class="swpm-login-action-msg text-primary">
            <span class="swpm-login-widget-action-msg"><?php echo $auth->get_message(); ?></span>
        </div>
        <div class="swpm-login-form-inner">
            <div class="swpm-username-label">
                <label for="swpm_user_name" class="authForm__label">Email*</label>
            </div>
            <div class="swpm-username-input">
                <input type="text" class="w-100 authForm__input swpm-text-field swpm-username-field" id="swpm_user_name" value="" size="25" name="swpm_user_name" />
            </div>
            <div class="swpm-password-label">
                <label for="swpm_password" class="authForm__label">Password*</label>
            </div>
            <div class="swpm-password-input">
                <input type="password" class="w-100 authForm__input authForm__input--pass swpm-text-field swpm-password-field" id="swpm_password" value="" size="25" name="swpm_password" />
            </div>
            <div class="swpm-before-login-submit-section"><?php echo apply_filters('swpm_before_login_form_submit_button', ''); ?></div>
            <div class="swpm-forgot-pass-link">
                <a id="forgot_pass" class="swpm-login-form-pw-reset-link"  href="<?php echo $password_reset_url; ?>">Glemt dit password?</a>
            </div>
            <div class="swpm-login-submit">
                <button type="submit" class="w-100 btn btn--primary btn--auth swpm-login-form-submit" name="swpm-login">Log ind</button>
            </div>
            <a href="#" class="switchModal btn btn--secondary btn--auth">Opret profil</a>
        </div>
    </form>
</div>
