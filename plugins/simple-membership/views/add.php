<?php
SimpleWpMembership::enqueue_validation_scripts(array('ajaxEmailCall' => array('extraData' => '&action=swpm_validate_email&member_id=' . filter_input(INPUT_GET, 'member_id'))));
$settings = SwpmSettings::get_instance();
$force_strong_pass = $settings->get_value('force-strong-passwords');
if (!empty($force_strong_pass)) {
    $pass_class = "validate[required,custom[strongPass],minSize[8]]";
} else {
    $pass_class = "";
}
?>
<div class="swpm-registration-widget-form">
    <form id="swpm-registration-form" class="authForm loginForm swpm-validate-form" name="swpm-registration-form" method="post" action="">
        <input type ="hidden" name="level_identifier" value="<?php echo $level_identifier ?>" />
        
            <label for="email" class="authForm__label">E-mail*</label>
            <input type="text" autocomplete="off" id="email" class="authForm__input validate[required,custom[email],ajax[ajaxEmailCall]]" value="<?php echo esc_attr($email); ?>" size="50" name="email" />
            
            <label for="password" class="authForm__label">Password*</label>
            <input type="password" autocomplete="off" id="password" class="authForm__input <?php echo $pass_class; ?>" value="" size="50" name="password" />

            <?php
                // echo $membership_level_alias; //Show the level name in the form.
                //Add the input fields for the level data.
                echo '<input type="hidden" value="' . $membership_level . '" size="50" name="membership_level" id="membership_level" />';
                //Add the level input verification data.
                $swpm_p_key = get_option('swpm_private_key_one');
                if (empty($swpm_p_key)) {
                    $swpm_p_key = uniqid('', true);
                    update_option('swpm_private_key_one', $swpm_p_key);
                }
                $swpm_level_hash = md5($swpm_p_key . '|' . $membership_level); //level hash
                echo '<input type="hidden" name="swpm_level_hash" value="' . $swpm_level_hash . '" />';
            ?>

  
            <?php
            //check if we need to display Terms and Conditions checkbox
            $terms_enabled = $settings->get_value('enable-terms-and-conditions');
            if (!empty($terms_enabled)) {
                $terms_page_url = $settings->get_value('terms-and-conditions-page-url');
                ?>
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <label><input type="checkbox" id="accept_terms" name="accept_terms" class="validate[required]" value="1"> <?php echo SwpmUtils::_('I accept the ') ?> <a href="<?php echo $terms_page_url; ?>" target="_blank"><?php echo SwpmUtils::_('Terms and Conditions') ?></a></label>
                    </td>
                </tr>
                <?php
            }
            //check if we need to display Privacy Policy checkbox
            $pp_enabled = $settings->get_value('enable-privacy-policy');
            if (!empty($pp_enabled)) {
                $pp_page_url = $settings->get_value('privacy-policy-page-url');
                ?>
                    <label class="acceptRegister">
                        <input type="checkbox" id="accept_pt" name="accept_pp" class="acceptRegister__input validate[required]" value="1">
                        <div class="acceptRegister__text">
                            <?php echo get_field('register_agreement', 'option') ?>
                        </div>
                    </label>
                <?php
            }
            ?>

        <div class="swpm-before-registration-submit-section" align="center"><?php echo apply_filters('swpm_before_registration_submit_button', ''); ?></div>

        <input type="hidden" name="action" value="custom_posts" />

        <button type="submit" value="<?php echo SwpmUtils::_('Register') ?>" class="btn btn--primary btn--auth swpm-registration-submit" name="swpm_registration_submit"><?php echo SwpmUtils::_('Register') ?></button>

    </form>
</div>
