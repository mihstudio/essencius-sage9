<?php
$auth = SwpmAuth::get_instance();
$user_data = (array) $auth->userData;
$user_data['membership_level_alias'] = $auth->get('alias');
extract($user_data, EXTR_SKIP);
$settings=SwpmSettings::get_instance();
$force_strong_pass=$settings->get_value('force-strong-passwords');
if (!empty($force_strong_pass)) {
    $pass_class="validate[custom[strongPass],minSize[8]]";
} else {
    $pass_class="";
}
SimpleWpMembership::enqueue_validation_scripts();
//The admin ajax causes an issue with the JS validation if done on form submission. The edit profile doesn't need JS validation on email. There is PHP validation which will catch any email error.
//SimpleWpMembership::enqueue_validation_scripts(array('ajaxEmailCall' => array('extraData'=>'&action=swpm_validate_email&member_id='.SwpmAuth::get_instance()->get('member_id'))));
?>
<div class="swpm-edit-profile-form">
    <form id="swpm-editprofile-form" class="authForm" name="swpm-editprofile-form" method="post" action="" class="swpm-validate-form">
        <?php wp_nonce_field('swpm_profile_edit_nonce_action', 'swpm_profile_edit_nonce_val') ?>
        
        <div class="form-row">
            <div class="col">
                <label for="email" class="authForm__label">Navn</label>
                <input class="authForm__input" id="first_name" value="<?php echo $first_name; ?>" size="50" name="first_name">
            </div>
            <div class="col">
                <label for="email" class="authForm__label">Elternavn</label>
                <input type="text" class="authForm__input" id="last_name" value="<?php echo $last_name; ?>" size="50" name="last_name">
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <label for="email" class="authForm__label">Email</label>
                <input type="text" class="authForm__input" id="email" name="email" value="<?php echo $email; ?>">
            </div>
        </div>
        <?php apply_filters('swpm_edit_profile_form_before_username', ''); ?>
        <?php apply_filters('swpm_edit_profile_form_before_submit', ''); ?>
        <div class="form-row">
            <div class="col text-right">
                <button type="submit" value="<?php echo SwpmUtils::_('Update') ?>" class="btn btn--primary swpm-edit-profile-submit" name="swpm_editprofile_submit">Rediger</button>
            </div>
        </div>
        <?php echo SwpmUtils::delete_account_button(); ?>

        <input type="hidden" name="action" value="custom_posts" />

    </form>
</div>