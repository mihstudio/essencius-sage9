<?php if (have_rows('sections')) : ?>
    <?php while (have_rows('sections')) : the_row(); ?>
        <?php if (get_row_layout() == 'hero') :
            $image = get_sub_field('image');
            $title = get_sub_field('title');
            $text = get_sub_field('text');
            $link_to = get_sub_field('link_to');
        ?>
            <section class="hero">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="hero__bg" style="background-image: url(<?php echo $image['url'] ?>);">
                                <div class="hero__content">
                                    <h1 class="hero__title"><?php echo $title ?></h1>
                                    <div class="hero__text"><?php echo $text ?></div>
                                    <div class="hero__btn">
                                        <a href="<?php echo $link_to['url'] ?>" class="btn btn--primary">
                                            <?php echo $link_to['title'] ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        <?php elseif (get_row_layout() == 'two_box') :
            $image = get_sub_field('image');
            $title = get_sub_field('title');
            $title_box = get_sub_field('title_sec_box');
            $text = get_sub_field('text');
            $text_box = get_sub_field('text_sec_box');
            $button = get_sub_field('button');
            $extra_text = get_sub_field('extra_text');
        ?>
            <section class="two-box">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-xl-6 mh-100">
                            <div class="two-box__half">
                                <div class="section-content">
                                    <h2 class="title-section"><?php echo $title ?></h2>
                                    <div class="two-box__half-text"><?php echo $text ?></div>
                                    <button type="button" class="btn btn--primary" data-toggle="modal" data-target="#modalAuth">
                                        <?php echo $button ?>
                                    </button>
                                    <div class="two-box__half-extratext"><?php echo $extra_text ?></div>
                                </div>
                                <div class="two-box__half-bg" style="background-image: url(<?php echo $image['url'] ?>);"></div>
                            </div>
                        </div>
                        <div class="col-12 col-xl-6 mt-xl-0 mt-5 d-inline-flex">
                            <div class="section-content">
                                <h2 class="title-section"><?php echo $title_box ?></h2>
                                <?php $i = 1;
                                if (have_rows('info_graphic')) : ?>
                                    <div class="info">
                                        <?php while (have_rows('info_graphic')) : the_row();
                                            $image = get_sub_field('icon');
                                            $title = get_sub_field('text');
                                        ?>
                                            <div class="info__elem order-<?php if ($i % 2 == 0) {
                                                                                echo '3';
                                                                            } else {
                                                                                echo '1';
                                                                            } ?>" style="background-image: url(<?php echo $image['url'] ?>);">
                                                <h3 class="info__title"><?php echo $title ?></h3>
                                            </div>

                                        <?php $i++;
                                        endwhile; ?>
                                        <div class="special-item order-2">
                                            <span class="special-item__title"> = </span>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="two-box__half-text"><?php echo $text_box ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php elseif (get_row_layout() == 'history') :
            $image = get_sub_field('image');
            $title = get_sub_field('title');
            $text = get_sub_field('text');
            $button = get_sub_field('button');
        ?>
            <section class="history">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-6 col-xl-8 pr-lg-0">
                            <div class="history__bg" style="background-image: url(<?php echo $image['url'] ?>);"></div>
                        </div>
                        <div class="col-12 col-lg-6 col-xl-4 pl-lg-0">
                            <div class="section-content">
                                <h2 class="title-section"><?php echo $title ?></h2>
                                <div class="two-box__half-text"><?php echo $text ?></div>
                                <a href="<?php echo $button['url'] ?>" class="btn btn--primary" target="_blank">
                                    <?php echo $button['title'] ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>