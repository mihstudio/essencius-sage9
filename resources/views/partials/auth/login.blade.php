@php
$auth = SwpmAuth::get_instance();
$setting = SwpmSettings::get_instance();
$password_reset_url = $setting->get_value('reset-page-url');
@endphp
@if(!SwpmMemberUtils::is_member_logged_in())
<button type="button" class="btn btn--outline" data-toggle="modal" data-target="#modalAuth">
  Log ind
</button>

<div class="modal fade modalAuth {{ (!empty($auth->get_message())) ? 'authError' : '' }}" id="modalAuth">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content modalContent">
      <div class="modal-header modalHeader">
        <button type="button" class="close modalHeader__close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body modalBody">
        <section class="login">
          <h2 class="modalBody__header">Log ind</h2>
          {!! do_shortcode('[swpm_login_form]') !!}
        </section>
      </div>
    </div>
  </div>
</div>
@else
<a href="{{ $setting->get_value('profile-page-url') }}" class="btn btn--primary">
  Min profil
</a>
<a href="{{ $setting->get_value('login-page-url') }}/?swpm-logout=true" class="btn btn--outline">
  Log ud
</a>
@endif
