@if(!SwpmMemberUtils::is_member_logged_in())
<?php
SimpleWpMembership::enqueue_validation_scripts(array('ajaxEmailCall' => array('extraData' => '&action=swpm_validate_email&member_id=' . filter_input(INPUT_GET, 'member_id'))));
$settings = SwpmSettings::get_instance();
$force_strong_pass = $settings->get_value('force-strong-passwords');
if (!empty($force_strong_pass)) {
    $pass_class = "validate[required,custom[strongPass],minSize[8]]";
} else {
    $pass_class = "";
}
?>

<button type="button" class="btn btn--primary" data-toggle="modal" data-target="#modalAuthRegister">
  Opret profil
</button>

<div class="modal fade modalAuth" id="modalAuthRegister">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content modalContent">
      <div class="modal-header modalHeader">
        <button type="button" class="close modalHeader__close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body modalBody">
        <section class="register">
          <h2 class="modalBody__header">Opret profil</h2>
          {!! do_shortcode('[swpm_registration_form]') !!}
        </section>
      </div>
    </div>
  </div>
</div>
@endif