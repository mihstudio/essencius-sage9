@php $change_title = get_field('change_title_page');
$new_title = get_field('title_page'); @endphp

@if($change_title == true) 
@php echo '<h1 class="mainContent__header mainContent__header--profile">' . $new_title . '</h1>' @endphp
@else
<h1 class="mainContent__header mainContent__header--profile">{!! App::title() !!}</h1>
@endif
