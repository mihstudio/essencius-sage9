<header class="headerNav">
  <nav class="navbar navbar-expand-md">
    <div class="container">
      <a class="navbar-brand" href="{{ bloginfo('url') }}">
        <img class="img-fluid headerNav_logo" src="@asset('images/kanzi_logo.png')" />
      </a>
      <div class="ml-auto navbarMenu">
          @include('partials.auth.register')
          @include('partials.auth.login')
        </div>
      {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse navbarMenu" id="collapsibleNavbar">
        
      </div> --}}
    </div>  
  </nav>
</header>