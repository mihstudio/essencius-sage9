<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-9">
        @if (has_nav_menu('footer_navigation'))
          {!! wp_nav_menu([
            'theme_location' => 'footer_navigation',
            'menu_class' => 'footerMenu list-inline',
            'menu_id' => '',
            'depth' => 1,
            'walker' => new \App\NavWalkerFooter(),
            'echo' => false]) !!}
        @endif
      </div>
      <div class="col-12 col-md-3 footerLogo text-center">
        <img class="footerLogo__image" src="@asset('images/kanzi_logo.png')" />
      </div>
    </div>
  </div>
</footer>
