<?php if (have_rows('sections', 37)) : ?>
    <?php while (have_rows('sections', 37)) : the_row(); ?>

        <?php if (get_row_layout() == 'two_box') :
            $image = get_sub_field('image', 37);
            $title = get_sub_field('title', 37);
            $title_box = get_sub_field('title_sec_box', 37);
            $text = get_sub_field('text', 37);
            $text_box = get_sub_field('text_sec_box', 37);
            $button = get_sub_field('button', 37);
            $extra_text = get_sub_field('extra_text', 37);
        ?>
            <section class="two-box">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-xl-6 mh-100">
                            <div class="two-box__half">
                                <div class="section-content">
                                    <h2 class="title-section"><?php echo $title ?></h2>
                                    <div class="two-box__half-text"><?php echo $text ?></div>
                                    <button type="button" class="btn btn--primary" data-toggle="modal" data-target="#modalAuth">
                                        <?php echo $button ?>
                                    </button>
                                    <div class="two-box__half-extratext"><?php echo $extra_text ?></div>
                                </div>
                                <div class="two-box__half-bg" style="background-image: url(<?php echo $image['url'] ?>);"></div>
                            </div>
                        </div>
                        <div class="col-12 col-xl-6 mt-xl-0 mt-5 d-inline-flex">
                            <div class="section-content">
                                <h2 class="title-section"><?php echo $title_box ?></h2>
                                <?php $i = 1;
                                if (have_rows('info_graphic')) : ?>
                                    <div class="info">
                                        <?php while (have_rows('info_graphic')) : the_row();
                                            $image = get_sub_field('icon');
                                            $title = get_sub_field('text');
                                        ?>
                                            <div class="info__elem order-<?php if ($i % 2 == 0) {
                                                                                echo '3';
                                                                            } else {
                                                                                echo '1';
                                                                            } ?>" style="background-image: url(<?php echo $image['url'] ?>);">
                                                <h3 class="info__title"><?php echo $title ?></h3>
                                            </div>

                                        <?php $i++;
                                        endwhile; ?>
                                        <div class="special-item order-2">
                                            <span class="special-item__title"> = </span>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="two-box__half-text"><?php echo $text_box ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        <?php endif; ?>
    <?php endwhile; ?>
<?php endif; ?>