<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class("front") @endphp>
    @php do_action('get_header') @endphp
    <section class="wrapper maintenance">
      
      <main class="mainContent">
        @yield('content')
      </main>
      @php do_action('get_footer') @endphp
    </section>
    @php wp_footer() @endphp
  </body>
</html>
