{{--
  Template Name: Konkurrence
--}}

@extends('layouts.app')

@section('content')
@php
$image = get_field('image');
$title = get_field('title');
$text = get_field('text');
$button = get_field('button');
@endphp
<section class="two-box">
  <div class="container">
    <div class="row">
      <div class="col-12 mh-100">
        <div class="two-box__half">
          <div class="section-content">
            <h2 class="title-section"><?php echo $title ?></h2>
            <div class="two-box__half-text"><?php echo $text ?></div>
            <button type="button" class="btn btn--primary" data-toggle="modal" data-target="#modalAuth">
              <?php echo $button ?>
            </button>
          </div>
          <div class="two-box__half-bg" style="background-image: url(<?php echo $image['url'] ?>);"></div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
@endsection