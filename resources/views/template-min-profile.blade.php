{{--
  Template Name: Min profil
--}}

@extends('layouts.app')

@section('content')
@if(is_user_logged_in())
<div class="container">
  <div class="row">
    <div class="col-12">
      @include('partials.page-min-profile-header')
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 coder">
      <h2 class="mainContent__header">Mine koder</h2>
      {!! do_shortcode('[kanzi-codes-profile]') !!}
    </div>
    <div class="col-md-6 offset-md-3">
    <h2 class="mainContent__header">Mine OPLYSNINGER</h2>
      @while(have_posts()) @php the_post() @endphp
        @include('partials.content-page')
      @endwhile
    </div>
  </div>
</div>
@endif
@include('partials.section-two-box')
@endsection
