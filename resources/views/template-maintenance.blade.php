{{--
  Template Name: Maintenance
--}}

@extends('layouts.maintenance')

@section('content')
<section class="underConstruction">
  <h1 class="underConstruction__header">{!! get_field('underHeader') !!}</h1>
  <div class="underConstruction__text">{{ get_field('underText') }}</div>
  <img class="img-fluid underConstruction__logo" src="@asset('images/kanzi_logo@2x.png')" />
</section>
@endsection
