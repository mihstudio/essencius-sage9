@extends('layouts.app')

@section('content')
  

  @if (!have_posts())
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-6">
      @include('partials.page-header')
        <div class="alert alert-warning">
      {{ __('Beklager, men den side, du forsøgte at se, findes ikke.', 'sage') }}
    </div>
    <a href="/" class="btn btn-primary">Gå tilbage til startsiden</a>
      </div>
    </div>
  </div>
    
  @endif
@endsection
