<?php

add_action('acf/init', 'my_acf_op_init');
function my_acf_op_init() {

    if( function_exists('acf_add_options_page') ) {

        $parent = acf_add_options_page(array(
            'page_title'  => __('Theme settings'),
            'menu_title'  => __('Theme settings'),
            'redirect'    => false,
        ));
    }
}