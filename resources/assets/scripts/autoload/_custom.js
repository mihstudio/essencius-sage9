$(document).ready(() => {
    $('.switchModal').on('click', function () {
        $('#modalAuth').modal('hide');
        $('#modalAuthRegister').modal('show');
    });

    if($('#modalAuth').hasClass('authError')) {
        $('#modalAuth').modal('show');
        $('#modalAuth').removeClass('authError');
    }
});
